import React from 'react';
import {StyleSheet, View, ImageBackground, Text, TouchableOpacity} from 'react-native';
import {FlatGrid} from 'react-native-super-grid';

import img1 from '../assets/images/foxes/large.jpg';
import img2 from '../assets/images/foxes/large(1).jpg';
import img3 from '../assets/images/foxes/large(2).jpg';
import img4 from '../assets/images/foxes/large(3).jpg';
import img5 from '../assets/images/foxes/large(4).jpg';
import img6 from '../assets/images/foxes/large(6).jpg';
import img7 from '../assets/images/foxes/large(7).jpg';
import img8 from '../assets/images/foxes/large(8).jpg';
import img9 from '../assets/images/foxes/large(9).jpg';
import img10 from '../assets/images/foxes/large(10).jpg';

export default class ImagesGrid extends React.Component {
    static navigationOptions = {
        header: null,
    };

    render() {
        const items = [{src: img1}, {src: img2}, {src: img3}, {src: img4}, {src: img5}, {src: img6}, {src: img7}, {src: img8}, {src: img9}, {src: img10}];
        return (
            <FlatGrid
                itemDimension={130}
                items={items}
                style={styles.gridView}
                // staticDimension={300}
                // fixed
                // spacing={20}
                renderItem={({item, index}) => (
                    <ImageBackground style={styles.picture} source={item.src}>
                        <TouchableOpacity/>
                    </ImageBackground>
                )}
            />

        )
    }
}

const styles = StyleSheet.create({
    gridView: {
        marginTop: 20,
        flex: 1,
    },
    itemContainer: {
        justifyContent: 'flex-end',
        borderRadius: 5,
        padding: 10,
        height: 150,
    },
    itemName: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '600',
    },
    itemCode: {
        fontWeight: '600',
        fontSize: 12,
        color: '#fff',
    },
    picture: {
        flex: 1,
        width: null,
        height: 150,
        resizeMode: 'contain',
    },
});

