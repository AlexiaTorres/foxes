import React, {Component} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';

import img1 from '../assets/images/foxes/large.jpg';
import img2 from '../assets/images/foxes/large(1).jpg';
import img3 from '../assets/images/foxes/large(2).jpg';
import img4 from '../assets/images/foxes/large(3).jpg';
import img5 from '../assets/images/foxes/large(4).jpg';
import img6 from '../assets/images/foxes/large(6).jpg';
import img7 from '../assets/images/foxes/large(7).jpg';
import img8 from '../assets/images/foxes/large(8).jpg';
import img9 from '../assets/images/foxes/large(9).jpg';
import img10 from '../assets/images/foxes/large(10).jpg';

import PhotoGrid from "react-native-thumbnail-grid";

const images = [img1, img2, img3, img4, img5, img6, img7, img8, img9, img10];
export default class Favorites extends Component {
    render() {
        return (
            <PhotoGrid source={images}/>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    }
});